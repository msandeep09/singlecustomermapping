﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DU;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Text;
using System.Web.Script.Services;

namespace WebApplication2
{

    public partial class _Default : Page
    {

        //[HttpPost]
        //[AllowAnonymous]
        DataUtility du = new DataUtility();
        public DataView dvPerson;
        public DataSet dsPerson;
        private String ticket = "sandeep.mishra";


        protected void Page_Load(object sender, EventArgs e)
        {
            String empID = "sandeep.mishra";
            //HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            //String username = ticket.Name;
            //String query = "SELECT count(*) FROM dbo.EmpIDMapping where EmpID='" + username + "'and AccessStatus=1";
            //DataSet ds = du.getDataset(query);
            //int access = ds.Tables[0].Rows.Count;

            //if (access != 1)
            //    Response.Redirect("http://intranet.music-group.com/");

            if (!IsPostBack)
            {
                BindData("");
            }

            //this.searchBox.Attributes.Add("onkeypress", "button_click(this,'" + this.searchBtn.ClientID + "')");
        }

        private void BindData(string searchTxt)
        {
            if (searchTxt == "")
            {
                dsPerson = new DataSet();
                string strSelectCmd = "SELECT  * FROM dbo.DimSingleCustomerMapping order by DateUpdated desc";
                dsPerson = du.getDataset(strSelectCmd);
                dvPerson = dsPerson.Tables[0].DefaultView;
                GridView1.DataSource = dvPerson;
                GridView1.DataBind();
            }
            else
            {
                dsPerson = new DataSet();
                string query = "exec advSearch '"+searchBox.Text+"'";
                //string strSelectCmd = "SELECT * FROM dbo.DimSingleCustomerMapping where CustomerName like '%"+searchBox.Text+"%' or CustomerID like '%"+searchBox.Text+"%' or " +
                //    "BusinessID like '%"+searchBox.Text+"%' order by DateUpdated desc";
                dsPerson = du.getDataset(query);
                dvPerson = dsPerson.Tables[0].DefaultView;
                GridView1.DataSource = dvPerson;
                GridView1.DataBind();
            }
        }

        protected void EditCustomer(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindData(this.searchBox.Text);
        }
        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindData(this.searchBox.Text);
        }
        protected void UpdateCustomer(object sender, GridViewUpdateEventArgs e)
        {
            string busID = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("BusinessID")).Text;
            string custID = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("CustomerID")).Text;
            string customerName = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("CustomerName")).Text;
            string country = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("CountryCode")).Text;
            string id = ((Label)GridView1.Rows[e.RowIndex].FindControl("ID")).Text;
            //HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
            String query = @"exec insertupdateid " + id + ", '" + busID + "','" + custID + "','" + customerName + "','" + country + "','" + ticket + "';";

            if (du.executequery(query) != 0)
            {
                GridView1.EditIndex = -1;
                BindData(this.searchBox.Text);
            }
            else
            {
                //alert messgae to be written;
            }
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            BindData("");
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected void DeleteCustomer(object sender, EventArgs e)
        {
            GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
            //row.RowIndex
            string id = ((Label)GridView1.Rows[row.RowIndex].FindControl("ID")).Text;
            string query = @"exec insertupdateid " + id + ", '','','','','" + ticket + "';";
            du.executequery(query);
            GridView1.EditIndex = -1;
            BindData("");
        }

        //protected void OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    customerN.Text = DropDownList1.SelectedItem.Value;
        //}

        protected void AddNewCustomer(object sender, EventArgs e)
        {
            //HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
            String query = @"exec insertupdateid '" + "','" + businessP.Text + "','" + customerN.Text + "','" + customerName.Text + "','" + country.Text + "','" + ticket + "';";
            int netCall = du.executequery(query);
            if (netCall != 0)
            {
                successID.Visible = true;
                successID.ForeColor = System.Drawing.ColorTranslator.FromHtml("#666666");
                successID.Text = "Successfully Added";
                BindData("");
            }
            else
            {
                successID.Visible = true;
                successID.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF3300");
                successID.Text = "Internal Error";
            }
        }
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public int fn(String s)
        {
            id1.Text = "Yooooooooooooooooooo";
            return 2; 

            //BindData(s);
        }

        protected void searchCustID(object sender, EventArgs e)
        {
            if (searchBox.Text == "") {
                
            }
            else
            {
                BindData(searchBox.Text);
            }
        }

        protected void Unnamed_Click1(object sender, EventArgs e)
        {
            Response.Redirect("http://localhost:65279/Default.aspx");
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            //Session.Abandon();
            //HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            //authCookie.Expires = DateTime.Now.AddDays(-10);
            //authCookie.Value = null;
            //HttpContext.Current.Response.SetCookie(authCookie);
            //Response.Redirect("http://intranet.music-group.com/");
        }

        protected void exportToCSV(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds = du.getDataset("SELECT  * FROM dbo.DimSingleCustomerMapping order by DateUpdated desc");
            var csv = new StringBuilder();
            var firstrow = string.Format(@"{0},{1},{2},{3},{4}", "ID", "BusinessID", "CustomerID", "Name of Customer", "Country Code");
            csv.AppendLine(firstrow);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                var newrow = string.Format(@"{0},{1},{2},{3},{4}", dr["ID"], dr["BusinessID"], dr["CustomerID"], dr["CustomerName"], dr["CountryCode"]);
                csv.AppendLine(newrow);
            }

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=SqlExport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv.ToString());
            Response.Flush();
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}