﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication2.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="mainContent" runat="server">

    <div class="jumbotron">
        <div id="container">
            <img Style="margin-left: 42.5%;"  src="download.jpg" alt="Music Group" style="width:120px;height:35px;"><br>
            <%--<asp:Label Style="margin-left: 40%; text-align: center; font-size: 20px;" ID="Label1" runat="server" Text="Login to Music Group"></asp:Label><br>--%>
            <asp:Label Style="margin-left: 41.7%; text-align: center; font-size: 14px;" ID="Label2" runat="server" Text="Single Customer Mapping"></asp:Label><br>
            <br>
            <div style="padding: 10px; margin-left: 31%">
                <asp:TextBox required Style="padding-left:0.5%; padding-right:0.5%;text-align: center; font-size: 12px;" ID="userID" Placeholder="Username" runat="server"></asp:TextBox>
                <asp:TextBox required TextMode="Password" Style="padding-right:0.5%;padding-left:0.5%;text-align: center; font-size: 12px;" ID="password" Placeholder="Password" runat="server"></asp:TextBox>
                <asp:Button Style="width: 64px; text-align: center; font-size: 12px;" ID="loginBtn" runat="server" Text="Login" OnClick="loginBtn_Click" />
            </div>
            <asp:Label Style="margin: 32%; text-align: center; font-size: 10px;font-family:monospace" ID="loginSuccessLabel" runat="server" Visible="False" ForeColor="#FF3300"></asp:Label>
        </div>
    </div>
</asp:Content>