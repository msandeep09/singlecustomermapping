﻿<%@ Page Title="Update Record" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <img src="logo.png" alt="Music Group" style="position: fixed; z-index: 10000; width: 104px; top: 10px; left: 45%; height: 31px;">

    <div class="jumbotron">
        <div id="container">
            <asp:LinkButton OnClick="Unnamed_Click" Style="font-size: 16px; z-index: 10000; position: fixed; right: 7%; top: 16px" ID="logout" class="glyphicon" runat="server">&#xe163;</asp:LinkButton>
            <asp:LinkButton OnClick="Unnamed_Click1" Style="font-size: 16px; z-index: 10000; position: fixed; left: 7%; top: 16px;" class="glyphicon" runat="server">&#xe021;</asp:LinkButton>

            <asp:Label ID="id1" Style="font-size: 18px;" Text="Single Customer Mapping" runat="server" Font-Bold="True" Font-Underline="False" BorderWidth="0px" Width="302px"></asp:Label>
            <asp:Label Text="sandeep.mishra" ID="userID" Style="z-index: 10000; position: fixed; right: 8.5%; top: 10.5px; padding: 0px; margin-left: 0.5%; margin-right: 4%; width: 64px; margin-top: 2.8px; color: lightgray; font-family: monospace; text-align: center; font-size: 12px; float: right;"
                runat="server"></asp:Label>

            <%--<asp:requiredfieldvalidator Style="position: relative; right: 15.5%; top: 10%; padding: 0px; margin-left: 0.5%; margin-right: 4%; width: 64px; margin-top: 2.8px; background-color: darkgray; color: white; text-align: center; 
            font-size: 12px; float: right;"  id="RequiredFieldValidator2"
      controltovalidate="searchBox"
      validationgroup="myVal"
      errormessage="QQQQQQQQQQQ"
      runat="Server">
    </asp:requiredfieldvalidator>--%>
            <asp:Button class="btn search-field" CausesValidation="true" ValidationGroup="myVal" Style="position: fixed; right: 16.5%; top: 11px; z-index: 10000; margin-left: 0.5%; margin-right: 4%; width: 64px; background-color: darkgray; color: white; text-align: center; font-size: 11px; float: right;"
                ID="searchBtn" OnClick="searchCustID" runat="server" Text="Search" />
            <asp:TextBox ValidationGroup="myVal" Style="position: fixed; right: 26%; top: 12px; z-index: 10000; padding-left: 0.5%; padding-right: 0.5%; height: 28px; width: 148px; text-align: center; font-size: 12px; float: right"
                ID="searchBox" class="form-control" ClientIDMode="Static" Placeholder="Search Customer ID" runat="server"></asp:TextBox>

            <br>

            <div style="margin-left: 12%;">
                <asp:Label Style="margin-left: 38%; font-family: Arial; text-align: center; font-size: 12px;" ID="successID" runat="server" Visible="False" ForeColor="#333333"></asp:Label>
                <br>
                <asp:TextBox ID="businessP" Style="padding-left: 0.5%; padding-right: 0.5%; text-align: center; font-size: 12px; margin-right: 0.5%;" Placeholder="Business ID" runat="server"></asp:TextBox>
                <asp:TextBox Style="padding-left: 0.5%; padding-right: 0.5%; text-align: center; font-size: 12px; margin-right: 0.5%;" ID="customerN" Placeholder="Customer ID" runat="server"></asp:TextBox>
                <asp:TextBox Style="padding-left: 0.5%; padding-right: 0.5%; text-align: center; font-size: 12px; margin-right: 0.5%;" ID="customerName" Placeholder="Customer Name" runat="server"></asp:TextBox>
                <asp:TextBox Style="padding-left: 0.5%; padding-right: 0.5%; text-align: center; font-size: 12px; margin-right: 0.5%;" ID="country" Placeholder="Country" runat="server"></asp:TextBox>

                <asp:Button Style="width: 64px; text-align: center; font-size: 12px;" ID="custIDbtn" runat="server" Text="Add" OnClick="AddNewCustomer" />
                <asp:LinkButton title="Export to CSV" class="glyphicon glyphicon-download-alt" Style="margin-top: 5px; float: right; margin-bottom: 8px; margin-right: 28px; font-size: initial;" ID="xls" runat="server" OnClick="exportToCSV" />
            </div>

            <%--<br style="line-height: 24px">--%>

            <div id="dvGrid" style="padding: 12px; width: 100%">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ItemStyle-HorizontalAlign="center" Style="font-family: Arial; text-align: center;" ID="GridView1" runat="server"
                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" text-align="center" AutoGenerateColumns="False" Font-Names="Arial"
                            Font-Size="11pt" AlternatingRowStyle-BackColor="#999999" AlternatingRowStyle-HorizontalAlign="Center" AllowPaging="True"
                            OnPageIndexChanging="OnPaging" OnRowEditing="EditCustomer" OnRowUpdating="UpdateCustomer" OnRowCancelingEdit="CancelEdit"
                            HorizontalAlign="Center" PageSize="200" ViewStateMode="Enabled">

                            <Columns>
                                <asp:TemplateField ItemStyle-Width="150px" HeaderText="ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="ID" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" />
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="350px" HeaderText="BusinessID">
                                    <ItemTemplate>
                                        <asp:Label ID="busID" runat="server" Text='<%# Eval("BusinessID")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox Style="padding-left: 3%; padding-right: 3%; width: 100%" ID="BusinessID" runat="server" Text='<%# Eval("BusinessID")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="130px" />
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="350px" HeaderText="Customer ID">
                                    <ItemTemplate>
                                        <asp:Label ID="custID" runat="server" Text='<%# Eval("[CustomerID]")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox Style="padding-left: 3%; padding-right: 3%; width: 100%" ID="CustomerID" runat="server" Text='<%# Eval("CustomerID")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="130px" Wrap="True" />
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="350px" HeaderText="Name of customer">
                                    <ItemTemplate>
                                        <asp:Label ID="custName" runat="server" Text='<%# Eval("CustomerName")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox Style="padding-left: 2.5%; padding-right: 2.5%; width: 100%; text-align: center;" ID="CustomerName" runat="server" Text='<%# Eval("CustomerName")%>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle Width="710px" HorizontalAlign="Left" Wrap="True" />
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-Width="350px" HeaderText="Country" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:Label ID="country" runat="server" Text='<%# Eval("CountryCode")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox Style="padding-left: 3%; padding-right: 3%; width: 100%;" ID="CountryCode" runat="server" Text='<%# Eval("CountryCode")%>'></asp:TextBox>
                                    </EditItemTemplate>

                                    <HeaderStyle HorizontalAlign="Center" />

                                    <ItemStyle Width="125px" HorizontalAlign="center" VerticalAlign="Middle" />

                                </asp:TemplateField>

                                <asp:CommandField ButtonType="Image" EditText=" " ControlStyle-CssClass="glyphicon glyphicon-pencil" ShowEditButton="True"
                                    UpdateImageUrl="~/images/icons8-Checkmark-24.png" CancelImageUrl="~/images/icons8-Delete-24.png">
                                    <ControlStyle CssClass="glyphicon glyphicon-pencil"></ControlStyle>

                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" Width="128px" />
                                </asp:CommandField>

                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton title="Delete Record" class="glyphicon glyphicon-trash"
                                            ID="lnkRemove" runat="server"
                                            CommandArgument='<%# Eval("[CustomerID]")%>'
                                            OnClientClick="return confirm('Do you want to delete?')"
                                            OnClick="DeleteCustomer">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" Wrap="False" />
                                </asp:TemplateField>

                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            <PagerStyle HorizontalAlign="Center" BorderColor="White" VerticalAlign="Middle" Wrap="True" />
                            <AlternatingRowStyle BackColor="#CCCCCC" />
                            <EditRowStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#666666" BorderColor="#666666" ForeColor="White" HorizontalAlign="Right" VerticalAlign="Middle" />
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView1" />
                    </Triggers>

                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <div class="updateprog">
                            <div class="updateprog2">
                                <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                    <path fill="#000000" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                                        <animateTransform
                                            attributeName="transform"
                                            attributeType="XML"
                                            type="rotate"
                                            dur="1s"
                                            from="0 50 50"
                                            to="360 50 50"
                                            repeatCount="indefinite" />
                                    </path>
                                </svg>

                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>

            <style>
                tr {
                    font-size: 15px;
                }

                td {
                    font-size: 12px;
                }

                    td input[alt~="Update"] {
                        top: 8px;
                        margin-right: 8px;
                    }

                    td input[alt~="Cancel"] {
                        top: 8px;
                        margin-left: 8px;
                    }

                .glyphicon:hover {
                    color: #2a6496;
                }

                td input[alt~=" "] {
                    font-size: 16px;
                }

                th {
                    text-align: center;
                }

                td {
                    padding-left: 1%;
                    padding-right: 1%;
                }

                .updateprog {
                    position: fixed;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 9999;
                }

                .updateprog2 {
                    width: 100px;
                    height: 100px;
                    margin: auto;
                    margin-top: 300px;
                }
            </style>

            <script type="text/javascript">
                <%--function findItemInArray(b) {
                    var items = [['txt_delivery', '<%= txt_delivery.ClientID %>'], ['txt_route', '<%= txt_route.ClientID %>']];
                    for (var k = 0; k < items.length; k++) {
                        if (items[k][0] === b) {
                            return items[k][1];
                        }
                    }
                }--%>

                function check() {
                    var count = 0;
                    var bus = document.getElementById("<%=businessP.ClientID%>");
                    var searchB = $("#<%=businessP.ClientID %>");
                    var val = searchB.val();
                    var cust = document.getElementById("<%=customerN.ClientID%>");
                    var searchB1 = $("#<%=customerN.ClientID %>");
                    var val1 = searchB1.val();
                    var custName = document.getElementById("<%=customerName.ClientID%>");
                    var searchB2 = $("#<%=customerName.ClientID %>");
                    var val2 = searchB2.val();
                    var coun = document.getElementById("<%=country.ClientID%>");
                    var searchB3 = $("#<%=country.ClientID %>");
                    var val3 = searchB3.val();
                    if (val == '') {
                        bus.style.borderColor = '#ff0000 ';
                        count++;
                    } else bus.style.borderColor = "#ffffff"

                    if (val1 == '') {
                        cust.style.borderColor = "#ff0000 ";
                        count++;
                    } else cust.style.borderColor = "#ffffff"

                    if (val2 == '') {
                        custName.style.borderColor = "#ff0000";
                        count++;
                    } else custName.style.borderColor = "#ffffff"

                    if (val3 == '') {
                        coun.style.borderColor = "#ff0000";
                        count++;
                    } else coun.style.borderColor = "#ffffff"

                    if (count != 0) {
                        return false;
                    } else return true;
                }
               
                $(function () {
                    $("#<%=custIDbtn.ClientID %>").click(function (e) {
                        var cust = document.getElementById("<%=custIDbtn.ClientID%>");
                        var searchB1 = $("#<%=custIDbtn.ClientID %>");
                        var val1 = searchB1.val();
                        if (!check()) {
                            e.preventDefault();
                        }

                        if (e.keyCode == 13) {
                            if (!check()) {
                                e.preventDefault();
                            }
                        }
                    });
                });

                $(function () {
                    $("#<%=businessP.ClientID %>").keypress(function (e) {
                        var bus = document.getElementById("<%=businessP.ClientID%>");
                        var searchB = $("#<%=businessP.ClientID %>");
                        var val = searchB.val();
                        if (e.keyCode == 13) {
                            console.log('busClicked: ', check());
                            if (!check()) {
                                e.preventDefault();
                            }
                            else {
                                e.preventDefault();
                            }
                        }
                    });
                });

                $(function () {
                    $("#<%=customerN.ClientID %>").keypress(function (e) {
                        var cust = document.getElementById("<%=customerN.ClientID%>");
                        var searchB1 = $("#<%=customerN.ClientID %>");
                        var val1 = searchB1.val();
                        if (e.keyCode == 13) {
                            console.log('custIDClicked: ', check());
                            if (!check()) {
                                e.preventDefault();
                            }
                            else {
                                e.preventDefault();
                            }
                        }
                    });
                });

                $(function () {
                    $("#<%=customerName.ClientID %>").keypress(function (e) {
                        var custName = document.getElementById("<%=customerName.ClientID%>");
                        var searchB2 = $("#<%=customerName.ClientID %>");
                        var val2 = searchB2.val();
                        if (e.keyCode == 13) {
                            console.log('custNameClicked: ', check());
                            if (!check()) {
                                e.preventDefault();
                            }
                            else {
                                e.preventDefault();
                            }
                        }
                    });
                });

                $(function () {
                    $("#<%=country.ClientID %>").keypress(function (e) {
                        var coun = document.getElementById("<%=country.ClientID%>");
                        var searchB3 = $("#<%=country.ClientID %>");
                        var val3 = searchB3.val();
                        if (e.keyCode == 13) {
                            console.log('countryClicked: ', check());
                            if (!check()) {
                                e.preventDefault();
                            }
                            else {
                                e.preventDefault();
                            }
                        }
                    });
                });

                $(function () {
                    $("#<%=searchBox.ClientID %>").keypress(function (e) {
                        var txt1 = document.getElementById("<%=searchBox.ClientID%>");
                        var searchB = $("#<%=searchBox.ClientID %>");
                        var val = searchB.val();
                        if (e.keyCode == 13) {
                            if (val == '') {
                                txt1.style.borderColor = '#ff0000';
                                e.preventDefault();
                                return false;
                            }
                            //console.log('get the search name', searchName);
                            //$.ajax({
                            //    url: "http://localhost:65279/Default/searchCustID",
                            //    type: 'POST',
                            //    contentType: 'application/json',
                            //    data: { 'name': searchName },
                            //    success: function (response) {
                            //        console.log('check the data response', response.content);
                            //    },
                            //    error: function (error) {
                            //        console.log('checking the error log', error.d);
                            //    }
                            //});
                            else {
                                $('#<%=searchBtn.ClientID%>').click();
                                return true;
                            }
                        }
                    });
                });

                            //console.log('get the search name', searchName);
                            //$.ajax({
                            //    url: "http://localhost:65279/Default/searchCustID",
                            //    type: 'POST',
                            //    contentType: 'application/json',
                            //    data: { 'name': searchName },
                            //    success: function (response) {
                            //        console.log('check the data response', response.content);
                            //    },
                            //    error: function (error) {
                            //        console.log('checking the error log', error.d);
                            //    }
                            //});


                //$(".chosen-container").bind('keyup click', function () {
                //    if ($(".chosen-results").children('li').hasClass('no-results')) {
                //        alert("Please Select the value only from dropdown");
                //        //var righttk = $('.chosen-results .no-results span').text();
                //        $('.chosen-single span').text(righttk);
                //        flag = 1;
                //        return false;
                //    }
                //    else {
                //        alert("Please Select the value only from dropdown");
                //        flag = 0;
                //        return true;
                //    }
                //});

<%--                $("#<%=DropDownList1.ClientID%>").chosen().change(function () {
                    //var temp = $("#<%=DropDownList1.ClientID%>").chosen().val();
                    $("#<%=customerN.ClientID%>").val(temp);
                });--%>

                //var config = {
                //    '.chosen-select': {},
                //    '.chosen-select-deselect': { allow_single_deselect: true },
                //    '.chosen-select-no-single': { disable_search_threshold: 10 },
                //    '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                //    '.chosen-select-width': { width: "95%" }
                //}

                //for (var selector in config) {
                //    $(selector).chosen(config[selector]);
                //}
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
    </div>
</asp:Content>