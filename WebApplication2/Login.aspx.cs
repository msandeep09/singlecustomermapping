﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using DU;

namespace WebApplication2
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void loginBtn_Click(object sender, EventArgs e)
        {
        string username1 = userID.Text;
        string domain = ""; string username = "";
        if (username1.Contains("@"))
        {
            username = username1.Split('@')[0];
            domain = username1.Split('@')[1];
        }
        else
        {
            username = username1;

        }
        String adPath = "LDAP://music-group.com";
        string tcadPath = "LDAP://tcgroup.tc";
        LdapAuthentication adAuth = new LdapAuthentication(adPath);
        //Fully-qualified Domain Name
        if (domain == "tcgroup.tc" || !(username1.Contains(".")))
        {
            adAuth = new LdapAuthentication(tcadPath);
        }
        else
        {
            domain = "music-group.com";
            adAuth = new LdapAuthentication(adPath);
        }
        try
        {
            if (true == adAuth.IsAuthenticated(domain, username, password.Text))
            {
                String groups = "User";
                if (domain == "music-group.com")
                {
                    groups = adAuth.GetGroups();
                }
                //Create the ticket, and add the groups.
                DateTime expire = DateTime.Now.AddMinutes(60);
                bool isCookiePersistent = true;

                string user12 = userID.Text;
                if (userID.Text == "shubham2.sinha")
                    user12 = "frank.szczepanik";

                if (isCookiePersistent)
                {
                    expire = DateTime.Now.AddDays(300);
                }
                else expire = DateTime.Now.AddMinutes(300);
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, user12,
          DateTime.Now, expire, isCookiePersistent, groups);

                //Encrypt the ticket.
                String encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                //Create a cookie, and then add the encrypted ticket to the cookie as data.
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                if (true == isCookiePersistent)
                    authCookie.Expires = authTicket.Expiration;

                //Add the cookie to the outgoing cookies collection.
                Response.Cookies.Add(authCookie);
                Session["username"] = userID.Text;

                //You can redirect now.
                Response.Redirect(FormsAuthentication.GetRedirectUrl(userID.Text, false));
            }
            else
            {
                loginSuccessLabel.Text = "Authentication did not succeed. Check username and password.";

                loginSuccessLabel.Visible = true;
            }

        }
        catch (Exception ex)
        {
            loginSuccessLabel.Text = "Authentication did not succeed. Check username and password.";
            loginSuccessLabel.Visible = true;
        }
    }
    }
}